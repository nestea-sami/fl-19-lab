const fs = require('fs');
const fsp = require('fs/promises');
const path = require('path');
const filesFolder = path.resolve('./files');

const getFileExtension = (filename) => {
  const filenameArr = filename.split('.');
  return filenameArr[filenameArr.length - 1];
};

const supportedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

function createFile (req, res, next) {
  try {
    const { filename, content } = req.body;
    if (filename === undefined) {
      res.status(400).send({ message: "Please specify 'filename' parameter" });
      return;
    }

    if (content === undefined) {
      res.status(400).send({ message: "Please specify 'content' parameter" });
      return;
    }

    const extension = getFileExtension(filename);

    if (supportedExtensions.includes(extension)) {
      fs.writeFile(`${filesFolder}/${filename}`, content, (err) => {
        if (err) {
          res.status(500).send({ message: 'Server error' });
          return;
        }
      });
      res.status(200).send({ message: 'File created successfully' })
    } else {
      res.status(400).send({
        message: `File extention should be one of the followings: ${supportedExtensions.map((ext) => ext)}`,
      });
    }
  } catch (error) {
    res.status(500).send({ message: 'Server error' });
  }
}


function getFiles (req, res, next) {
  const filesList = [];
  try {
    fs.readdir(filesFolder, (err, result) => {
      if (err) {
        res.status(400).json({message: 'Client error'});
      }
      result.forEach(file => {
        filesList.push(file);
      })
      res.status(200).send({
        "message": "Success",
        "files": filesList});
    })
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
}

const getFile = async (req, res, next) => {
  try {
    let uploadedDate;
    let extension = path.extname(req.url).split('.')[1];
    fs.stat(filesFolder + req.url, (error, stats) => {
      if (error) {
        console.log(error);
        return;
      }
      uploadedDate = stats.birthtime;
    });
    res.status(200).send({
      "message": "Success",
      "filename": path.basename(req.url),
      "content": await fsp.readFile(filesFolder + req.url, { encoding: 'utf8' }),
      "extension": extension,
      "uploadedDate": uploadedDate});
  } catch (error) {
    res.status(400).json({message: 'Server error'});
  }
}

function editFile (req, res, next) {
  try {
    const { filename, content } = req.body;
    if (filename === undefined) {
      res.status(400).send({ message: "Please specify 'filename' parameter" });
      return;
    }

    if (content === undefined) {
      res.status(400).send({ message: "Please specify 'content' parameter" });
      return;
    }

    fs.stat(path.join(filesFolder, filename), (err, stats) => {
      if (err) {
        res.status(400).send({ message: `No file with '${filename}' filename found` })
        return
      }

      fs.writeFile(path.join(filesFolder, filename), content, (err) => {
        if (err) {
          res.status(500).send({ message: 'Server error' })
          return
        }

        res.status(200).send({ message: `${filename} is successfully updated` })
      })
    })
  } catch (error) {
    res.status(500).send({ message: 'Server error' })
  }
}

function deleteFile (req, res, next) {
  try {
    const { filename } = req.params;

    fs.unlink(path.join(filesFolder, filename), (err) => {
      if (err) {
        res.status(400).send({ message: `No file with '${filename}' filename found` });
        return;
      }

      res.status(200).send(`${filename} is successfully deleted`);
    })
  } catch (error) {
    res.status(500).send({ message: 'Server error' });
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
  getFileExtension
}
